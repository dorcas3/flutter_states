import 'package:flutter/material.dart';
import 'pages/secondpage.dart';
import 'pages/thirdpage.dart';
import 'package:provider/provider.dart';
import './classes/ItemList.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
   return ChangeNotifierProvider(
    create: (context) => ItemListModel(),
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => SecondPage(),
        '/third': (context) => ThirdPage()
      },
     ),
   );
  }
}