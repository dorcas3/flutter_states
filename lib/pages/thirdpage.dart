import 'package:flutter/material.dart';
import 'package:flutter_states/classes/ItemList.dart';
import 'package:provider/provider.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  // List<Language> stacks = [
  //   Language(stack:'',environ:'')
  // ];

  List stacks = [
    ['flutter', 'assets/images/f.png', 'Mobile'],
    ['Adonis', 'assets/images/adonis.jpeg', 'Backend'],
    ['Vue', 'assets/images/vue.png', 'Frontend'],
    ['Quarks', 'assets/images/quarkus.png', 'Backend'],
    ['Go', 'assets/images/go.png', 'Backend'],
    ['Sql', 'assets/images/sql.png', 'Database'],
  ];

  @override
  Widget build(BuildContext context) {
    var finalData = context.watch<ItemListModel>();

    return Scaffold(
      appBar: AppBar(
        title: Text(
        finalData.getappBar,
          style: TextStyle(color: Colors.white),
        ),
        //backgroundColor: Colors.blue[400],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 0.0),
            child: Column(
              children: [
                Card(
                  // margin: EdgeInsets.symmetric(horizontal: 15,vertical: 4),
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Row(
                      
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        
                        Text(finalData.getName,style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
                        Text(finalData.getStack,style: TextStyle(fontSize: 20.0,fontWeight: FontWeight.bold),),
                      ],
                    ),
                  ),
                ),
                GridView.count(
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  childAspectRatio: (2 / 3),
                  children: stacks
                      .map((data) => GestureDetector(
                          child: Card(
                              elevation: 5.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              color: Colors.teal[50],
                              child: Center(
                                  child: Column(
                                children: [
                                  Image.asset(data[1]),
                                  Text(data[0],
                                      style: TextStyle(
                                          fontSize: 22,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w700),
                                      textAlign: TextAlign.center),
                                  Text(data[2],
                                      style: TextStyle(
                                          fontSize: 15,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w300),
                                      textAlign: TextAlign.center),
                                ],
                              )))))
                      .toList(),
                ),
              ],
            ),
          ),
        ),
      ),
      backgroundColor: Colors.grey[200],
    );
  }
}
