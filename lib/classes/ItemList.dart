import 'package:flutter/material.dart';

class ItemListModel with ChangeNotifier {
  String _name = '';
  String _stacks = '';
  String _appBar = '';

  String get getName => _name;
  String get getStack => _stacks;
  String get getappBar => _appBar;

  setData(name,stacks,appBar) {
    _name = name;
    _stacks = stacks;
    _appBar = appBar;
    notifyListeners();
  }
}
