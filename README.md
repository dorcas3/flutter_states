# flutterstates

Flutter Widgets

## Description

A flutter Application that demonstrates the use of states in flutter

## Screenshots Preview

![Image Preview](assets/images/data.png)

## Getting Started

### User Requirements

- The logo and name should be passed in all the app bars
- The items that you collect on the second page should be passed to the third and displayed before the grid view you built on the third page.


## Prerequisites
- Flutter v2.2.0

## Setup and Installations

- Clone the repository into your local machine
- In the terminal type flutter doctor to install all the dependencies
- Type flutter run command to launch the application

### License

* LICENSED UNDER  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](license/MIT)
